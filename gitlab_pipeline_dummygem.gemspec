# frozen_string_literal: true

require_relative 'lib/gitlab_pipeline_dummygem/version'

Gem::Specification.new do |spec|
  spec.name = 'gitlab_pipeline_dummygem'
  spec.version = GitlabPipelineDummygem::VERSION
  spec.authors = ['Rémi Piotaix']
  spec.email = ['remi@piotaix.fr']

  spec.summary = 'Dummy project to try automatic push to rubygems from gitlab pipelines'
  spec.homepage = 'https://gitlab.com/piotaixr/gitlab_pipeline_dummygem'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/piotaixr/gitlab_pipeline_dummygem'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.glob(%w[bin/**/* lib/**/* *.gemspec])
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
