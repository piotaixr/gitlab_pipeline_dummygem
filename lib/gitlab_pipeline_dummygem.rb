# frozen_string_literal: true

require 'gitlab_pipeline_dummygem/version'

module GitlabPipelineDummygem
  class Error < StandardError; end
  # Your code goes here...
end
